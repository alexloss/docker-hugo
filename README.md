# Docker-Hugo

## About Docker-Hugo

A Docker image to build and optionally serve [Hugo](https://gohugo.io/) sites, designed to use for local development as well as  in a CI/CD pipeline.

## Install

`$ docker pull registry.gitlab.com/gitlab-ci-utils/docker-hugo`

The following Docker image tags are available:

- `{version}`: Tags for specific Hugo versions (e.g. `0.53`, `0.54.0`, `0.55.6`)
- `latest`: Tag corresponding to the latest commit to master (may not yet be released as a version)

## Usage

### GitLab CI

The following is an example job from a `.gitlab-ci.yml` file to use this image to build a Hugo site.  In the case the site is located in the site/ folder at the root of the project, and the output is saved to the public/ folder at the root of the project, which would be used for publication with [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/).

``` yml
pages:
  image: registry.gitlab.com/gitlab-ci-utils/docker-hugo:latest
  variables:
    SITE: '$CI_PROJECT_DIR/site'
    DEST: '$CI_PROJECT_DIR/public'
  script:
    - hugo --cleanDestinationDir --source $SITE --destination $DEST
  artifacts:
    paths:
    - public
```

All available Docker image tags can be found in the `gitlab-ci-utils/docker-hugo` repository at https://gitlab.com/gitlab-ci-utils/docker-hugo/container_registry.  The `latest` image is rebuilt daily to incorporate OS updates and the latest Hugo release. Tags for the latest patch of each Hugo minor release are available for at least a year. All other image tags may be removed at any time.

**Note:** Any images in the `gitlab-ci-utils/docker-hugo/tmp` repository are temporary images used during the build process and may be deleted at any point.

### Local Development

The image is designed to facilitate local development use through the Docker CLI in addition to use in a CI/CD pipeline, which allows use of Hugo without maintaining a local installation.

#### Hugo Build

The following command builds a Hugo site in the current working directory through the Docker CLI.

`$ docker container run --rm -v ${pwd}:/site registry.gitlab.com/gitlab-ci-utils/docker-hugo:latest`

#### Hugo Server

The following command starts a Hugo server for a site in the current working directory through the Docker CLI, exposed locally on port 1313 (i.e. it can be viewed at http://localhost:1313/).

`$ docker container run --rm -it -p 1313:1313 -v ${pwd}:/site registry.gitlab.com/gitlab-ci-utils/docker-hugo:latest hugo server --bind 0.0.0.0 --renderToDisk`

#### Interactive Terminal

The following command starts a container with an interactive terminal to run Hugo commands in the current working directory.  If a server is started, it is exposed locally on port 1313 (i.e. it can be viewed at http://localhost:1313/).

`$ docker container run --rm -it -p 1313:1313 -v ${pwd}:/site registry.gitlab.com/gitlab-ci-utils/docker-hugo:latest sh`

The following Hugo command aliases are available in the terminal:

|  Alias  | Description                                  | Command                      |
|---------|-----------------------------------------------|------------------------------|
| `hugob` | Hugo build clearing the destination folder   | `hugo --cleanDestinationDir` |
| `hugos` | Run Hugo server within the container with the appropriate bindings to view on the host and rendering files to disk so they can be opened on the host from a mounted volume | `hugo server --bind 0.0.0.0 --renderToDisk` |
| `hugov` | Show Hugo version                            | `hugo version`               |
