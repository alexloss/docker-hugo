FROM alpine:3.17.2@sha256:ff6bdca1701f3a8a67e328815ff2346b0e4067d32ec36b7992c1fdc001dc8517 as temp

ARG VERSION

ADD https://github.com/gohugoio/hugo/releases/download/v${VERSION}/hugo_extended_${VERSION}_Linux-64bit.tar.gz /tmp/

WORKDIR /tmp

RUN tar -zxvf hugo*.tar.gz &&   \
    mv hugo /bin/ &&            \
    rm -rf ./* &&               \
    hugo version

FROM alpine:3.17.2@sha256:ff6bdca1701f3a8a67e328815ff2346b0e4067d32ec36b7992c1fdc001dc8517

ENV ENV="/etc/profile"
ENV SITE="/site"

COPY alias.sh /etc/profile.d/

COPY --from=temp /bin/hugo /bin/hugo

WORKDIR $SITE

EXPOSE 1313

LABEL org.opencontainers.image.licenses="Apache-2.0"
LABEL org.opencontainers.image.source="https://gitlab.com/gitlab-ci-utils/docker-hugo"
LABEL org.opencontainers.image.title="docker-hugo"
LABEL org.opencontainers.image.url="https://gitlab.com/gitlab-ci-utils/docker-hugo"

CMD ["hugo", "--cleanDestinationDir"]
