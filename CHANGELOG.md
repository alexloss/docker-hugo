# Changelog

## Unreleased

### Changed

- Added standard set of `LABEL`s to image. (#22)

### Miscellaneous

- Added `lint-sh` job to CI pipeline.

## v1.2.0 (2021-10-07)

### Changed

- Move to container build/test/deploy pipeline leveraging `kaniko` for build and `skopeo` for deploy. (#18)

## v1.1.0 (2021-09-15)

### Miscellaneous

- Setup [renovate](https://docs.renovatebot.com/) for dependency updates, including configuring tracking Hugo versions in CI pipeline (#16, #17)
- Removed daily rebuild, originally intended to get Hugo or Docker updates, but those are now created as needed by [renovate](https://docs.renovatebot.com/). The philosophy for image tags has not changed:
  - The `latest` image is rebuilt as needed to incorporate OS/Hugo updates
  - Tags for the latest patch of each Hugo minor release are available for at least a year
  - All other image tags may be removed at any time

## v1.0.0 (2021-08-08)

### Changed

- BREAKING: Updated philosophy for this project to version releases only for changes to the project itself, not each Hugo release. The `latest` image is rebuilt daily to incorporate OS updates and the latest Hugo release. Tags for the latest patch of each Hugo minor release are available for at least a year. All other image tags may be removed at any time. (#15)

## v0.86.1 (2021-08-01)

### Changed

- Updated to Hugo v0.86.1

## v0.86.0 (2021-07-21)

### Changed

- Updated to Hugo v0.86.0

### Miscellaneous

- Updated pipeline to perform image smoke test (#14)

## v0.85.0 (2021-07-05)

### Changed

- Updated to Hugo v0.85.0

## v0.84.4 (2021-07-01)

### Changed

- Updated to Hugo v0.84.4

## v0.84.3 (2021-06-29)

### Changed

- Updated to Hugo v0.84.3

## v0.84.2 (2021-06-28)

### Changed

- Updated to Hugo v0.84.2

## v0.84.1 (2021-06-24)

### Changed

- Updated to Hugo v0.84.1

## v0.84.0 (2021-06-18)

### Changed

- Updated to Hugo v0.84.0

## v0.83.1 (2021-05-02)

### Changed

- Updated to Hugo v0.83.1

## v0.83.0 (2021-05-01)

### Changed

- Updated to Hugo v0.83.0
- Updated license to Apache 2.0 for consistency with Hugo

## v0.82.1 (2021-04-21)

### Changed

- Updated to Hugo v0.82.1

## v0.82.0 (2021-03-21)

### Changed

- Updated to Hugo v0.82.0

### Miscellaneous

- Updated CI pipeline to replace `only`/`except` with `rules` (#13)

## v0.81.0 (2021-02-19)

### Changed

- Updated to Hugo v0.81.0

## v0.80.0 (2020-12-31)

### Changed

- Updated to Hugo v0.80.0

## v0.79.1 (2020-12-20)

### Changed

- Updated to Hugo v0.79.1

## v0.79.0 (2020-11-27)

### Changed

- Updated to Hugo v0.79.0

### Miscellaneous

- Updated CI pipeline to leverage simplified include syntax in GitLab 13.6 (#12)

## v0.78.2 (2020-11-13)

### Changed

- Updated to Hugo v0.78.2

## v0.78.1 (2020-11-05)

### Changed

- Updated to Hugo v0.78.1

## v0.78.0 (2020-11-03)

### Changed

- Updated to Hugo v0.78.0

## v0.77.0 (2020-10-30)

### Changed

- Updated to Hugo v0.77.0

## v0.76.5 (2020-10-14)

### Changed

- Updated to Hugo v0.76.5

## v0.76.4 (2020-10-12)

### Changed

- Updated to Hugo v0.76.4

## v0.76.3 (2020-10-08)

### Changed

- Updated to Hugo v0.76.3

## v0.76.2 (2020-10-07)

### Changed

- Updated to Hugo v0.76.2

## v0.76.0 (2020-10-06)

### Changed

- Updated to Hugo v0.76.0

## v0.75.1 (2020-09-15)

### Changed

- Updated to Hugo v0.75.1

## v0.75.0 (2020-09-14)

### Changed

- Updated to Hugo v0.75.0

## v0.74.3 (2020-07-23)

### Changed

- Updated to Hugo v0.74.3

## v0.74.2 (2020-07-17)

### Changed

- Updated to Hugo v0.74.2

## v0.74.1 (2020-07-13)

### Changed

- Updated to Hugo v0.74.1

## v0.73.0 (2020-06-23)

### Changed

- Updated to Hugo v0.73.0

## v0.72.0 (2020-05-31)

### Changed

- Updated to Hugo v0.72.0

## v0.71.1 (2020-05-25)

### Changed

- Updated to Hugo v0.71.1

## v0.71.0 (2020-05-18)

### Changed

- Updated to Hugo v0.71.0

## v0.70.0 (2020-05-06)

### Changed

- Updated to Hugo v0.70.0

## v0.69.2 (2020-04-24)

### Changed

- Updated to Hugo v0.69.2

## v0.69.1 (2020-04-22)

### Changed

- Updated to Hugo v0.69.1

## v0.69.0 (2020-04-11)

### Changed

- Updated to Hugo v0.69.0

## v0.68.3 (2020-03-24)

### Changed

- Updated to Hugo v0.68.3

## v0.68.1 (2020-03-22)

### Changed

- Updated to Hugo v0.68.1

## v0.68.0 (2020-03-21)

### Changed

- Updated to Hugo v0.68.0

## v0.67.1 (2020-03-15)

### Changed

- Updated to Hugo v0.67.1

## v0.67.0 (2020-03-09)

### Changed

- Updated to Hugo v0.67.0

## v0.66.0 (2020-03-03)

### Changed

- Updated to Hugo v0.66.0

## v0.65.3 (2020-02-23)

### Changed

- Updated to Hugo v0.65.3

## v0.65.1 (2020-02-20)

### Changed

- Updated to Hugo v0.65.1

## v0.64.0 (2020-02-04)

### Changed

- Updated to Hugo v0.64.0

## v0.63.2 (2020-01-27)

### Changed

- Updated to Hugo v0.63.2

## v0.63.1 (2020-01-23)

### Changed

- Updated to Hugo v0.63.1

## v0.62.2 (2020-01-05)

### Changed

- Updated to Hugo v0.62.2

## v0.62.1 (2020-01-01)

### Changed

- Updated to Hugo v0.62.1

### Miscellaneous

- Added check to CI pipeline for new Hugo releases (#3)

## v0.62.0 (2019-12-23)

### Changed

- Updated to Hugo v0.62.0

## v0.61.0 (2019-12-11)

### Changed

- Updated to Hugo v0.61.0

## v0.60.1 (2019-12-02)

### Changed

- Updated to Hugo v0.60.1

## v0.59.1 (2019-10-31)

### Changed

- Updated to Hugo v0.59.1

## v0.59.0 (2019-10-21)

### Changed

- Updated to Hugo v0.59.0

## v0.58.3 (2019-09-19)

### Changed

- Updated to Hugo v0.58.3

## v0.58.2 (2019-09-13)

### Changed

- Updated to Hugo v0.58.2

## v0.58.1 (2019-09-06)

### Changed

- Updated to Hugo v0.58.1

## v0.58.0 (2019-09-05)

### Changed

- Updated to Hugo v0.58.0

## v0.57.2 (2019-08-17)

### Changed

- Updated to Hugo v0.57.2

## v0.57.1 (2019-08-16)

### Changed

- Updated to Hugo v0.57.1

## v0.56.3 (2019-07-31)

### Changed

- Updated to Hugo v0.56.3

## v0.56.2 (2019-07-30)

### Changed

- Updated to Hugo v0.56.2

## v0.56.1 (2019-07-28)

### Changed

- Updated to Hugo v0.56.1

## v0.56.0 (2019-07-25)

### Changed

- Updated to Hugo v0.56.0

## v0.55.6 (2019-05-18)

### Changed

- Updated to Hugo v0.55.6

## v0.55.5 (2019-05-03)

### Changed

- Updated to Hugo v0.55.5

## v0.55.4 (2019-04-25)

### Changed

- Updated to Hugo v0.55.4

## v0.55.3 (2019-04-20)

### Changed

- Updated to Hugo v0.55.3

## v0.55.2 (2019-04-17)

### Changed

- Updated to Hugo v0.55.2

## v0.55.1 (2019-04-13)

### Changed

- Updated to Hugo v0.55.0
- Updated CI pipeline to use template jobs, including GitLab built-in templates and Dockerfile linting

### Fixed

- Fixed issues in Dockerfile identified by Hadolint

## v0.54.0 (2019-02-03)

### Changed

- Updated to Hugo v0.54.0
- Added several additional Hugo command aliases to image
- Updated README with implementation details

## v0.53 (2018-01-26)

- Initial release with Hugo v0.53
